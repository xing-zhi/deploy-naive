# 前端代码部署脚本

在`deploy-scripts`目录下执行下面的命令即可
```bash
$ node ./deploy.js --source=<source folder> --target=<production folder>
```

接受两个参数

+ source：前端代码的根目录
+ target：生成环境前端代码的目录

# 示例
## 安装模块
在`deploy-scripts`目录下运行下面的命令会安装所有需要的node模块

```bash
$ npm run init
```

## 前后端代码在一个代码仓库里
### 示例项目名：fe-be-integrated

### 项目结构
	fe-be-integrated
	├── front    // 前端代码目录
	├── package.json
	├── public    // 生产环境前端代码目录
	└── server.js

### 部署
在`deploy-scripts`目录下运行

```bash
$ node ./deploy.js --source=../fe-be-integrated/front/ --target=../fe-be-integrated/public/
```

### 查看结果
在`fe-be-integrated`目录下运行

```bash
$ node server.js
```

然后浏览器打开提示的链接查看效果

## 前后端代码在两个代码仓库里
### 示例项目名：fe-be-integrated

### 项目结构
	fe-be-apart
	├── back-end
	│   ├── package.json
	│   ├── public    // 生产环境前端代码目录
	│   └── server.js
	└── front-end    // 前端代码目录

### 部署
在`deploy-scripts`目录下运行

```bash
$ node ./deploy.js --source=../fe-be-apart/front-end/ --target=../fe-be-apart/back-end/public/
```

### 查看结果
在`fe-be-apart/back-end`目录下运行
```bash
$ node server.js
```

然后浏览器打开提示的链接查看效果

# TODO
暂时只是一个示例，后续需要进一步改进

+ 增强扩展性，处理更多的项目结构
+ 增加错误处理机制
+ 完整的流程：从git拉取代码 -> 编译代码 -> 发布到相应服务器
