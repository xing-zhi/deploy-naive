fis.match('js/*.js', {
  // fis-parser-babel-6.x
  parser: fis.plugin('babel-6.x'),
  rExt: 'js'
});

fis.match('scss/(*.scss)', {
  isCssLike: true,
  // fis-parser-node-sass
  parser: fis.plugin('node-sass'),
  rExt: '.css',
  // change css folder from scss to css
  release: '/css/$1',
  postprocessor: fis.plugin('autoprefixer', {
    browsers: ['> 1%', 'last 2 versions'],
    flexboxfixer: true,
    gradientfixer: true
  }),
  useSprite: true
});

fis.match('::package', {
    spriter: fis.plugin('csssprites', {
      margin: 10,
      layout: 'matrix'
    })
  });

fis.media('prod')
  .match('*.{js, scss, css, png, jpeg, jpg}', {
    useHash: true
  })
  .match('*.{scss, css}', {
    optimizer: fis.plugin('clean-css')
  })
  .match('*.js', {
    optimizer: fis.plugin('uglify-js')
  })
  .match('*.html', {
    optimizer: fis.plugin('dfy-html-minifier', {
      removeComments: true,
      collapseWhitespace: true,
      conservativeCollapse: true,
      removeAttributeQuotes: true,
      minifyJS: true,
      minifyCSS: true
    })
  })
  .match('*.png', {
    optimizer: fis.plugin('png-compressor')
  });
