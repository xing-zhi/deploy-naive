const path = require('path');
const childProcess = require('child_process').exec;

const shelljs = require('shelljs');

const pathResolve = ((...args) => path.resolve.apply(null, [__dirname, ...args]));

shelljs.cd(pathResolve('../fe-be-apart/back-end'));
childProcess('npm i', () => {
  shelljs.cd(pathResolve('../fe-be-apart/front-end'));
  childProcess('npm i', () => {
    shelljs.cd(pathResolve('../fe-be-integrated/'));
    childProcess('npm i', () => {
      shelljs.cd(pathResolve('../fe-be-integrated/front'));
      childProcess('npm i', () => {
        console.log('所有的模块安装完毕');
      });
    });
  });
});
