const path = require('path');
const childProcess = require('child_process').exec;

const shelljs = require('shelljs');
const argv = require('yargs').argv;

const sourceFolder = path.resolve(__dirname, argv.source);
const target = path.resolve(__dirname, argv.target);

const pwd = shelljs.pwd().stdout;

shelljs.cd(sourceFolder);
childProcess(`npm run release`, () => {
  console.log('前端代码构建成功');
  shelljs.cd(pwd);
  console.log('开始移动文件到指定目录')
  childProcess(`node ./mv-files --source=${sourceFolder} --target=${target}`, () => {
    console.log('文件移动完成');
  });
});
