const path = require('path');

const shelljs = require('shelljs');
const argv = require('yargs').argv;

const pathResolve = ((...args) => path.resolve.apply(null, [__dirname, ...args]));

const sourceFolder = argv.source;
const targetFolder = argv.target;

const targets = ['js', 'css', 'imgs', 'scss', '*.html'].map(p => pathResolve(targetFolder, p));
const sources = ['js', 'css', 'imgs', 'scss', '*.html'].map(p => pathResolve(sourceFolder, 'dist', p));

targets.forEach((target) => shelljs.rm('-rf', target));
sources.forEach((source) => shelljs.cp('-R', source, targetFolder));
